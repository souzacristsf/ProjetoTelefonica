 <?php
header('Access-Control-Allow-Origin: *');
require 'Slim/Slim.php';

$app = new Slim();
$app->get('/usuarios','getUsuario');
$app->get('/operadoras','getOperadora');
$app->post('/usuarios', 'addUsuario');
$app->delete('/usuarios/:id','deleteUsuario');
$app->run();

function getUsuario() {
	$sql = "select * FROM usuario ORDER BY nome";
	try {
		$db = getConnection();
		$stmt = $db->query($sql);  
		$usuarios = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo '{"usuario": ' . json_encode($usuarios) . '}}';
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getOperadora() {
	$sql = "select * FROM operadora ORDER BY nome";
	try {
		$db = getConnection();
		$stmt = $db->query($sql);  
		$operadoras = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo '{"operadora": ' . json_encode($operadoras) . '}}';
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}



function addUsuario() {
	error_log('addUsuario\n', 3, '/var/tmp/php.log');
	$request = Slim::getInstance()->request();
	$usuario = json_decode($request->getBody());
	$sql = "INSERT INTO usuario (name, telefone, data, idOperadora) VALUES (:name, :telefone, :data, :idOperadora)";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bindParam("name", $usuario->name);
		$stmt->bindParam("telefone", $usuario->grapes);
		$stmt->bindParam("data", $usuario->country);
		$stmt->bindParam("idOperadora", $usuario->region);
		$stmt->execute();
		$usuario->id = $db->lastInsertId();
		$db = null;
		echo json_encode($usuario); 
	} catch(PDOException $e) {
		error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function deleteUsuario($id) {
	$sql = "DELETE FROM usuario WHERE id=:id";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$db = null;
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getConnection() {
	$dbhost="localhost";//127.0.0.1
	$dbuser="root";
	$dbpass="";
	$dbname="apptelefonia";
	$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);	
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $dbh;
}
